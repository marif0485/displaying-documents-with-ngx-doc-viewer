import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent {
  title = "ngx-doc-viewer";
  name = "Angular";
  pdfSrc = "http://www.pdf995.com/samples/pdf.pdf";
  pptSrc = "https://acdbio.com/sites/default/files/sample.ppt";
  excelSrc = "https://go.microsoft.com/fwlink/?LinkID=521962";
  showPDF: boolean = true;
  showPPT: boolean = false;
  showExcel: boolean = false;

  toggleViewer(view) {
    this.showPDF = this.showPPT = this.showExcel = false;
    switch (view) {
      case "pdf":
        this.showPDF = true;
        break;
      case "ppt":
        this.showPPT = true;
        break;
      case "excel":
        this.showExcel = true;
        break;
      default: 
        this.showPDF = true;
    }
  }
}
